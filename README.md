# Proyecto IPC: Arquitectura Cliente-Servidor Multi-Protocolo

Este proyecto implementa una arquitectura cliente-servidor multi-protocolo utilizando varios mecanismos de IPC como sockets. El objetivo es diseñar, implementar y probar aplicaciones en C que aprovechen los mecanismos de IPC proporcionados por el Sistema Operativo.

## Descripción del Proyecto

![Diagrama de Bloques](img/block_diagram.jpg)

### Objetivo
El proyecto tiene como objetivo implementar una arquitectura cliente-servidor con al menos 3 protocolos de la familia <sys/socket.h>. El servidor debe manejar múltiples clientes a través de varios protocolos, registrar conteos de datos por protocolo y el total de datos recibidos en archivos persistentes.

### Detalles de Desarrollo
La implementación incluye tres pares de programas cliente/servidor inicialmente para diferentes configuraciones de sockets: INET (IPV4), UNIX e INET6 (IPV6). Estos servidores luego se fusionarán en un único servidor capaz de manejar todos los protocolos.

### Estructura de Hilos
El servidor inicia hilos dedicados para aceptar conexiones para cada protocolo (Server_INET_Stream, Server_UNIX_Stream, Server_INET6_Stream). Para cada conexión, un hilo de manejo de tareas gestiona la recepción y acumulación de datos por protocolo, mientras que un hilo de recuento asegura la gestión del ciclo de vida del hilo.

### Características Clave
- Manejo dinámico de múltiples clientes a través de los protocolos INET, UNIX e INET6.
- Registro preciso y persistencia de recuentos de datos recibidos.
- Arquitectura basada en hilos que garantiza un manejo eficiente de conexiones y acumulación de datos.

## Instrucciones de Uso
- Compila y ejecuta el código del lado del servidor (make testServer).
- Compila el código del lado del cliente.
- El Makefile del lado del cliente incluye pruebas individuales para todos los tipos de cliente o una prueba global usando un archivo bash (administra permisos antes de usar esa opción).

### Empezar
Para inicializar el proyecto:
- Clona este repositorio.
- Compila el código usando las banderas de compilación proporcionadas.
- Suministra los parámetros necesarios al servidor para especificar direcciones, puertos, etc.

### Colaboración y Pruebas
- Invita a miembros del equipo y colaboradores utilizando las funciones de colaboración de GitLab.
- Crea solicitudes de fusión, habilita aprobaciones y configura entornos protegidos para pruebas.

### Integración CI/CD
Utiliza las capacidades de CI/CD de GitLab para pruebas automatizadas y análisis de código. Configura despliegues para Kubernetes, Amazon EC2 o ECS.

### Estado del Proyecto
Este proyecto se mantiene activamente y da la bienvenida a contribuciones. Consulta CONTRIBUTING.md para conocer las pautas de contribución.

## Instalación y Dependencias
Asegúrate de cumplir con las siguientes dependencias antes de ejecutar el código:
- Compilador de C
- Bibliotecas <sys/socket.h>

## Licencia 
Este proyecto está licenciado bajo la [Licencia MIT](LICENSE.md).
