#ifndef INETSOCKETCONFIG_H    // Comienzo del ifdef guard
#define INETSOCKETCONFIG_H_H

void ServerConfigSocketINET(int *sock, struct sockaddr_in *servaddr, int iport, long unsigned int max, char *stringaddr);

#endif // Fin del ifdef guard