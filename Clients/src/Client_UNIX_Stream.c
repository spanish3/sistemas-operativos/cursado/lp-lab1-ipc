#include "Common.h"
//#include <strings.h>

void VerificarArgumentosClientUNIX(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configurar socket
    int sockfd; // File Descriptro para el socket
    struct sockaddr_un servaddr; // Estructura para espesificar el server address

    // Enviar/Recibir
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue;

    // Obtener mensajes por STDIN para enviar al server
    char string[MAXLINE];   // Ingresado por stdin
    char aux[MAXLINE];      // Sin /n
    char endOfMsg[7] = "\n";

    unsigned int sleepTime;
    long unsigned int timesSent;
    long unsigned int cont = 0;

    char file_path[MAXLINE]; // Path for the UNIX file
    char file_name[MAXLINE]; // Name for the UNIX file 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerificarArgumentosClientUNIX(argc, argv);

    // Crear Socket, AF_UNIX => Dentro de este sistema, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handlear el address (path del archivo) del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset( (char *)&servaddr, '\0', sizeof(servaddr) );
	servaddr.sun_family = AF_UNIX;
	strcpy(file_path, argv[1]);
    strcpy(file_name, file_path);
    strcat(file_name, "UNIX_FILE");
    /*if (remove(file_name) == 0)      
    {                               
        rmdir(file_path);
    }
    if (mkdir(file_path, 0777) < 0)
    {
        printf("Error creating the directory for the UNIX file\n");
        printf("The errno is %s\n", errnoname((int)errno));
        exit(EXIT_FAILURE);
    }*/
    strcpy(servaddr.sun_path, file_name);
    
    //strcpy( servaddr.sun_path, argv[1]);
	//servlen = strlen( servaddr.sun_path) + sizeof(servaddr.sun_family);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor y handlear errores
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Recibir por stdin los mensajes a enviar o detectar si se desea salir del programa
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    strcpy(string,argv[2]);
    timesSent = (unsigned long int)atoi(argv[3]);
    sleepTime = (unsigned int)atoi(argv[4]);

    while(1)
    {
        if(cont != timesSent)
        {
            usleep(sleepTime);
            // Construir el mensaje a enviar
            //----------------------------------------------------------------------------------------------------------------------------------------
            string[strcspn(string,"\n")] = 0;      
            strcpy(aux,string);                     // Guardar string sin \n en aux evaular el while
            strcat(string,endOfMsg);                // Añadir fin de mensaje (se simula como el final de un mensaje HTTP)

            // Enviar el mensaje
            //----------------------------------------------------------------------------------------------------------------------------------------
            sendBytes = strlen(string); // Calcular la cantidad de caracteres del mensaje
            // Enviar el mensaje:
            // ssize_t write(int fd, const void *buf, size_t count); Escribe hasta "count" Bytes del buffer comenzando en *buf en el archivo referido
            // por el file descriptor fd. Devuelve la cantidad de Bytes escritos en caso de exito o -1 en caso de error. Tambien puede devolver una 
            // cantidad de Bytes menor a count por alguna interrupcion u algun otro motivo, en cuyo caso tambien se handlea como un error
            WriteReturnValue = write(sockfd, string, sendBytes);
            if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
            {
                printf("Falla al enviar mensaje\n");
                exit(EXIT_FAILURE); 
            }
            //----------------------------------------------------------------------------------------------------------------------------------------

            cont++;
        }
        else
        {
            break;
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Cerrar el socket
    exit(EXIT_SUCCESS); // Salir
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosClientUNIX(int argc, char *argv[])
{
    // Verificar cantidad de argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 5)  
    {
        printf("cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el nombre archivo para la conexion UNIX sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((strlen(argv[1])>MAXLINE) || (!isValidFilename(argv[1])))
    {
        printf("Nombre de archivo incorrecto\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el mensaje ingresado conste solo de letras y numeros
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if(((isdigit(argv[2][i]) == 0) && (isalpha(argv[2][i]) == 0)) || strlen(argv[2]) > MAXLINE) 
        {                                                               
            printf("Debe ingresar una mensaje correcto\n");               
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la cantidad de veces a enviar el mensaje sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[3]); i ++)
    {
        if((isdigit(argv[3][i]) == 0)|| (atoi(argv[3]) <= 0)) 
        {                                                                
            printf("Debe ingresar una cantidad correcta\n");                
            exit(EXIT_FAILURE);
        }
    }                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la cantidad us a esperar antes de enviar de nuevo sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[4]); i ++)
    {
        if((isdigit(argv[4][i]) == 0)|| (atoi(argv[4]) <= 0)) 
        {                                                                
            printf("Debe ingresar una cantidad correcta\n");                
            exit(EXIT_FAILURE);
        }
    }                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------
}