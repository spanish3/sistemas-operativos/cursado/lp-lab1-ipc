#include "Common.h"
//#include <strings.h>

void VerificarArgumentosClient(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configurar socket
    int sockfd; // File Descriptro para el socket
    struct sockaddr_in servaddr; // Estructura para espesificar el server address
    short unsigned int iport; // int para verificar num de puerto

    // Enviar/Recibir
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue;

    // Obtener mensajes por STDIN para enviar al server
    char string[MAXLINE];   // Ingresado por stdin
    char aux[MAXLINE];      // Sin /n
    char endOfMsg[7] = "\n";

    unsigned int sleepTime;
    long unsigned int timesSent;
    long unsigned int cont = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerificarArgumentosClient(argc, argv);
    iport = (short unsigned int)atoi(argv[2]);  // en el argumento puede exceder el valor max del short unsigned int

    // Crear Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handlear el address del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // bzero(void *s, size_t n) elimina los datos en los n bytes de memoria comenzando en la dirección donde apunta s, escribiendo cero.
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    // htons (host to network, short) convierte cualquier tipo de orden de bytes al orden estandar de bytes de red. De esta forma, no importa
    // que formato de bytes tengan las aplicaciones que se conecten al socket, estas podrán comunicarse.
    servaddr.sin_port = htons(iport);
    // int inet_pton(int af, const char *restrict src, void *restrict dst); Donde af = Address Family, convierte el string src en una estructura 
    // de direccion de red (ej: servaddr) con el formato de af y luego copia esa estructura en dst. De esta manera obtenemos la direccion a la
    // que queremos conectarnos mediante los argumentos.
    if(inet_pton(AF_INET,argv[1],&servaddr.sin_addr) <= 0)
    {
        printf("Falla al convertir dir IP a binario\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor y handlear errores
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Recibir por stdin los mensajes a enviar o detectar si se desea salir del programa
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    strcpy(string,argv[3]);
    timesSent = (unsigned long int)atoi(argv[4]);
    sleepTime = (unsigned int)atoi(argv[5]);

    while(1)
    {
        if(cont != timesSent)
        {
            usleep(sleepTime);
            // Construir el mensaje a enviar
            //----------------------------------------------------------------------------------------------------------------------------------------
            string[strcspn(string,"\n")] = 0;      
            strcpy(aux,string);                     // Guardar string sin \n en aux evaular el while
            strcat(string,endOfMsg);                // Añadir fin de mensaje (se simula como el final de un mensaje HTTP)

            // Enviar el mensaje
            //----------------------------------------------------------------------------------------------------------------------------------------
            sendBytes = strlen(string); // Calcular la cantidad de caracteres del mensaje
            // Enviar el mensaje:
            // ssize_t write(int fd, const void *buf, size_t count); Escribe hasta "count" Bytes del buffer comenzando en *buf en el archivo referido
            // por el file descriptor fd. Devuelve la cantidad de Bytes escritos en caso de exito o -1 en caso de error. Tambien puede devolver una 
            // cantidad de Bytes menor a count por alguna interrupcion u algun otro motivo, en cuyo caso tambien se handlea como un error
            WriteReturnValue = write(sockfd, string, sendBytes);
            if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
            {
                printf("Falla al enviar mensaje\n");
                exit(EXIT_FAILURE); 
            }
            //----------------------------------------------------------------------------------------------------------------------------------------

            cont++;
        }
        else
        {
            break;
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Cerrar el socket
    exit(EXIT_SUCCESS); // Salir
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosClient(int argc, char *argv[])
{
    // Verificar cantidad de argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 6)  
    {
        printf("cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado en los argumentos sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) // Verificar que en el argumento para el puerto se haya
        {                                                                // ingresado un num y no letras ni caracteres especiales
            printf("Debe ingresar un puerto correcto\n");                // y que este num sea > = 0 y < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // El valor se escribe en iport LUEGO de verrificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el mensaje ingresado conste solo de letras y numeros
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[3]); i ++)
    {
        if(((isdigit(argv[3][i]) == 0) && (isalpha(argv[3][i]) == 0)) || strlen(argv[3]) > MAXLINE) 
        {                                                               
            printf("Debe ingresar una mensaje correcto\n");               
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la cantidad de veces a enviar el mensaje sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[4]); i ++)
    {
        if((isdigit(argv[4][i]) == 0)|| (atoi(argv[4]) <= 0)) 
        {                                                                
            printf("Debe ingresar una cantidad correcta\n");                
            exit(EXIT_FAILURE);
        }
    }                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la cantidad us a esperar antes de enviar de nuevo sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[5]); i ++)
    {
        if((isdigit(argv[5][i]) == 0)|| (atoi(argv[5]) <= 0)) 
        {                                                                
            printf("Debe ingresar una cantidad correcta\n");                
            exit(EXIT_FAILURE);
        }
    }                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
