#ifndef SERVERARGVERIFY_H    // Comienzo del ifdef guard
#define SERVERARGVERIFY_H_H

void verificarArgumentos(int argc, char *argv[]);
void VerificarArgumentosINET(char *argv[]);
void VerificarArgumentosUNIX(char *argv[]);
void VerificarArgumentosINET6(char *argv[]);

#endif // Fin del ifdef guard