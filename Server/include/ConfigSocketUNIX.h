#ifndef UNIXSOCKETCONFIG_H    // Comienzo del ifdef guard
#define UNIXSOCKETCONFIG_H_H

void ServerConfigSocketUNIX(int *sock, struct sockaddr_un *servaddr, long unsigned int max, char *filename);

#endif // Fin del ifdef guard