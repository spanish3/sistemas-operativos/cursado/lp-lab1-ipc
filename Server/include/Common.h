#ifndef COMMON_H    // Comienzo del ifdef guard
#define COMMON_H_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // write, close, etc
#include <ctype.h> // isdigit()
#include <arpa/inet.h> // inet_pton()
#include <sys/un.h> // direciiones unix
#include <sys/stat.h> // mkdir
#include <net/if.h> // INET6
#include <netinet/in.h> // INET6
#include <pthread.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>

#include "CountingThread.h"
#include "TaskHandlingThread.h"
#include "FileWritingThread.h"
#include "ConfigSocketINET.h"
#include "ConfigSocketUNIX.h"
#include "ConfigSocketINET6.h"
#include "Arg-Verify-Server.h"
#include "CountingThread.h"
#include "TaskHandlingThread.h"
#include "Server_INET_Stream.h"
#include "Server_UNIX_Stream.h"
#include "Server_INET6_Stream.h"

#define SA struct sockaddr
#define MAXLINE 4096

struct local_threads_arg_struct {
    int id;
    int checksegs;
    long unsigned int *Local_TotalBytesRcv;
    long unsigned int *Local_LastBytesRcv;
    long unsigned int *Global_TotalBytesRcv;
    long unsigned int *Global_LastBytesRcv;
    int *ConnSocket;
    int ExitThread;
    int *Handlers;
    pthread_mutex_t *lock;
    pthread_mutex_t *global_lock;
    int *salir;
};

struct local_writer_arg_struct {
    long unsigned int *TotalBytesRcv;
    long unsigned int *LastBytesRcv;
    char Write_File_Name[MAXLINE];
    pthread_mutex_t *lock;
    int *salir;
};

struct INET_arg_struct {
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    char INET_Write_File_Name[MAXLINE];
    pthread_mutex_t *global_lock;
    long unsigned int *Global_TotalBytesRcv;
    long unsigned int *Global_LastBytesRcv;
    int maxClientes;
    int *salir;
};

struct UNIX_arg_struct {
    char UNIX_File_Name[MAXLINE];
    char UNIX_Write_File_Name[MAXLINE];
    pthread_mutex_t *global_lock;
    long unsigned int *Global_TotalBytesRcv;
    long unsigned int *Global_LastBytesRcv;
    int maxClientes;
    int *salir;
};

struct INET6_arg_struct {
    char IPV6_Server_Address[MAXLINE];
    short unsigned int IPV6_iport;
    char IPV6_Interface[MAXLINE];
    char INET6_Write_File_Name[MAXLINE];
    pthread_mutex_t *global_lock;
    long unsigned int *Global_TotalBytesRcv;
    long unsigned int *Global_LastBytesRcv;
    int maxClientes;
    int *salir;
};
enum Status {
    Start = 1,
    Expire = 2
};

struct Info_t{
    atomic_int status;
    char filename[MAXLINE];
    FILE *destFile;
    long unsigned int *TotalBytesRcv;
    long unsigned int *LastBytesRcv;
    long unsigned int bandwidth;
    unsigned long int cont;
    pthread_mutex_t *lock;
    timer_t timerId;
} ;

    int isValidIPAddress(char *ipAddr);
    int isValidIPV6Address(char *ipAddr);
    int my_strlen(char *c);
    void clearBuffer();
    int safeGetString(char *string, int max);
    void ocuparHandler(int *Handlers, int i, pthread_mutex_t *lock);
    void liberarHandler(int *Handlers, int i, pthread_mutex_t *lock);
    int isValidFilename(char *string);
    int getFirstAvailableHandler(int *Handlers, long unsigned int maxHandlers);
    int getAvailableHandlersAmount(int *Handlers, long unsigned int maxHandlers);
    void Hndlr(union sigval sigev_value);
    void TimerInit(struct Info_t* info);
    char const * errnoname(int errno_);

#endif // Fin del ifdef guard