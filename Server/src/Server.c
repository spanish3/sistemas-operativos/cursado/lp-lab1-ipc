#include "Common.h"

int main(int argc, char *argv[])
{    
    // Variables: Hilos y estructuras de argumentos para los tres protocolos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t INET_Server_Thread;
    pthread_t UNIX_Server_Thread;
    pthread_t INET6_Server_Thread;

    struct INET_arg_struct INET_arguments;
    struct UNIX_arg_struct UNIX_arguments; 
    struct INET6_arg_struct INET6_arguments;

    char file_path[MAXLINE]; // Path for the UNIX file
    char file_name[MAXLINE]; // Name for the UNIX file 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables: Hilo para escribir archivo global con la cantidad de bytes recividos / estructura de argumentos para ese hilo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t Global_Filewriting_Thread;
    struct local_writer_arg_struct global_arguments;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables: Cantidad de bytes global recivida y lock para acceder/modificar esta en ex mutua
    //--------------------------------------------------------------------------------------------------------------------------------------------
    unsigned long int Global_TotalBytesRcv;
    unsigned long int Global_LastBytesRcv;
    pthread_mutex_t global_lock = PTHREAD_MUTEX_INITIALIZER;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Mecanismo para salir del programa
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char salir[MAXLINE];
    int exitAllThreads = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que todos los argumentos de main sean correctos y asignarlos 
    // al argumento correspondiente de la estructura para el hilo que los use.
    //--------------------------------------------------------------------------------------------------------------------------------------------
    verificarArgumentos(argc,argv);

    strcpy(INET_arguments.IPV4_Server_Address,argv[1]);
    INET_arguments.IPV4_iport = (short unsigned int)atoi(argv[2]);
    strcpy(INET_arguments.INET_Write_File_Name,argv[3]);
    INET_arguments.global_lock = &global_lock;
    INET_arguments.Global_TotalBytesRcv = &Global_TotalBytesRcv;
    INET_arguments.Global_LastBytesRcv = &Global_LastBytesRcv;
    INET_arguments.maxClientes = atoi(argv[10]);
    INET_arguments.salir = &exitAllThreads;

    strcpy(file_path, argv[4]);
    strcpy(file_name, file_path);
    strcat(file_name, "UNIX_FILE");
    if (remove(file_name) == 0)      
    {                               
        rmdir(file_path);
    }
    if (mkdir(file_path, 0777) < 0)
    {
        printf("Error creating the directory for the UNIX file\n");
        printf("The errno is %s\n", errnoname((int)errno));
        exit(EXIT_FAILURE);
    }
    strcpy(UNIX_arguments.UNIX_File_Name, file_name);
    strcpy(UNIX_arguments.UNIX_Write_File_Name,argv[5]);
    UNIX_arguments.global_lock = &global_lock;
    UNIX_arguments.Global_TotalBytesRcv = &Global_TotalBytesRcv;
    UNIX_arguments.Global_LastBytesRcv = &Global_LastBytesRcv;
    UNIX_arguments.maxClientes = atoi(argv[10]);
    UNIX_arguments.salir = &exitAllThreads;

    strcpy(INET6_arguments.IPV6_Server_Address,argv[6]);
    INET6_arguments.IPV6_iport = (short unsigned int)atoi(argv[7]);
    strcpy(INET6_arguments.IPV6_Interface,argv[8]);
    strcpy(INET6_arguments.INET6_Write_File_Name,argv[9]);
    INET6_arguments.global_lock = &global_lock;
    INET6_arguments.Global_TotalBytesRcv = &Global_TotalBytesRcv;
    INET6_arguments.Global_LastBytesRcv = &Global_LastBytesRcv;
    INET6_arguments.maxClientes = atoi(argv[10]);
    INET6_arguments.salir = &exitAllThreads;

    global_arguments.TotalBytesRcv = &Global_TotalBytesRcv;
    global_arguments.LastBytesRcv = &Global_LastBytesRcv;
    strcpy(global_arguments.Write_File_Name,argv[11]);
    global_arguments.salir = &exitAllThreads;
    global_arguments.lock = &global_lock;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Lanzar hilos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // FileWriter Global
    pthread_create(&Global_Filewriting_Thread,NULL,FileWritingThreadCode,&global_arguments);
    // INET
    pthread_create(&INET_Server_Thread,NULL,INET_Server_Code,&INET_arguments);
    // UNIX
    pthread_create(&UNIX_Server_Thread,NULL,UNIX_Server_Code,&UNIX_arguments);
    // INET6
    pthread_create(&INET6_Server_Thread,NULL,INET6_Server_Code,&INET6_arguments);
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    while(!exitAllThreads)
    {
        printf("Ingrese 'salir' para cerrar el programa servidor\n");
        safeGetString(salir,MAXLINE);
        if(strcmp(salir,"salir"))
        {
            exitAllThreads = 1;
            // join threads
        }
        else
        {
            printf("Ha ingresado %s\n",salir);
        }
    }
}

