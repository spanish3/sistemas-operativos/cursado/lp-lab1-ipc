#include "Common.h"

void* FileWritingThreadCode(void * arg)
{
    struct local_writer_arg_struct *arguments = arg;
    FILE* destFile;
    
    destFile = fopen(arguments->Write_File_Name, "w"); 
    fclose(destFile);
    
    struct Info_t t1;
    t1.status = Start;
    strcpy(t1.filename,arguments->Write_File_Name);
    t1.destFile = destFile;
    t1.TotalBytesRcv = arguments->TotalBytesRcv;
    t1.LastBytesRcv = arguments->LastBytesRcv;
    t1.cont = 1;
    t1.lock = arguments->lock;
    TimerInit(&t1);

    while(*(arguments->salir) == 0)
    {
        // nada
    }

    return NULL;
}