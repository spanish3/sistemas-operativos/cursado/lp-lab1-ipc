#include "Common.h"

void* ThreadCode(void * arg)
{
    struct local_threads_arg_struct *arguments = arg;
    int EA,EP,count = 0;
    //printf("Soy countingthread %d y recibi el sock %d\n",arguments->id,*(arguments->ConnSocket));
    
    while(1)
    {
        if(!arguments->ExitThread)
        {
            pthread_mutex_lock(arguments->lock);
            arguments->checksegs = 1;
            pthread_mutex_unlock(arguments->lock);
            EA = arguments->checksegs;
            usleep(1000000);
            EP = arguments->checksegs;
            count++;
            if(EA != EP)
            {
                count = 0;
            }
            //printf("\nSoy countingThread %d, la cuenta es %d\n",arguments->id,count);
            if(count == 10)
            {
                pthread_mutex_lock(arguments->lock);
                arguments->ExitThread = 1;
                pthread_mutex_unlock(arguments->lock);
                break;
            }
        }
    }

    //printf("Fin de counting thread %d\n",arguments->id);
    return NULL;
}