#include "Common.h"

void* Task(void * arg)
{
    struct local_threads_arg_struct *arguments = arg;
    //printf("Soy Task %d y recibi el sock %d, ExitTHread = %d, salir = %d\n",arguments->id,*(arguments->ConnSocket),arguments->ExitThread,*(arguments->salir));
    
    char recvline[MAXLINE+1]; // Buffer para recibir. Es MAXLINE + 1 porque...
    char aux[MAXLINE];         // Auxiliar para quitar el checksum y el end of messege
    char rawmsg[MAXLINE] = "";      // Mensaje sin checksum ni end of line
    long int readBytes;
    unsigned long int readBytesConvert;
    
    while(arguments->ExitThread == 0 && *(arguments->salir) == 0) 
    {
        // Leer mensaje: La llamada ssize_t read(int fd, void *buf, size_t count) intenta leer hasta count bytes del file descriptor fd.
        // Read devuelve la cantidad de bytes leidos. Si devuelve cero significa que es el final del archivo que nos envian.
        // Devuelve -1 en caso de error
        //------------------------------------------------------------------------------------------------------------------------------------
        while(readBytes <= 0)
        {
            if(!arguments->ExitThread)
            {
                readBytes = recv(*(arguments->ConnSocket), recvline, MAXLINE-1, MSG_DONTWAIT);
            }
            if(arguments->ExitThread) // Si se acaba el tiempo o se produce un error de lectura (que puede ser pot timeout), salir
            {
                if(readBytes < 0)
                {
                    printf("Error de lectura en task %d\n",arguments->id);
                    printf("El errno es %s\n",errnoname((int)errno));
                } 
                if(close(*(arguments->ConnSocket)) < 0)
                {
                    printf("Error al cerrar la conec\n");
                }
                break;
            }
        }
        if(!arguments->ExitThread && *(arguments->salir) == 0)
        {
            if(readBytes > 0)
            {
                pthread_mutex_lock(arguments->lock);
                arguments->checksegs = 0;
                pthread_mutex_unlock(arguments->lock);
                readBytesConvert = (unsigned long int)readBytes;
                if(readBytesConvert > 0)
                {
                    pthread_mutex_lock(arguments->lock);
                    *(arguments->Local_TotalBytesRcv) += readBytesConvert;
                    *(arguments->Local_LastBytesRcv) += readBytesConvert;
                    pthread_mutex_unlock(arguments->lock);
                    pthread_mutex_lock(arguments->global_lock);
                    *(arguments->Global_TotalBytesRcv) += readBytesConvert;
                    *(arguments->Global_LastBytesRcv) += readBytesConvert;
                    pthread_mutex_unlock(arguments->global_lock);
                    readBytesConvert = 0;
                }
            }
            while(readBytes > 0)
            {
                if(recvline[readBytes - 1] == '\n') // El final de los mensajes http suele tener \n\r\n\r, al detectar un \n podemos intuir 
                {                                   // que es el final del mensaje.
                    // Borrar contenido del string (colocar ceros) para que no quede parte de mensajes anteriores
                    //----------------------------------------------------------------------------------------------------------------------------
                    memset(aux, 0, MAXLINE);    
                    memset(rawmsg, 0, MAXLINE);
                    //----------------------------------------------------------------------------------------------------------------------------          

                    // Obtener el mensaje sin hash ni end of file
                    //----------------------------------------------------------------------------------------------------------------------------
                    strcpy(aux,recvline);
                    int i = 0;
                    while(aux[i] != '\n')
                    {
                        rawmsg[i] = aux[i];
                        i++;
                    }
                    //printf("Mensaje recibido por task %d: %s\n",arguments->id,rawmsg);
                    //----------------------------------------------------------------------------------------------------------------------------
                    readBytes = 0;
                    break;
                }
            }
        }
        else
        {
            break;
        }
        
    }

    //printf("Termina task: %d\n",arguments->id);
    liberarHandler(arguments->Handlers,arguments->id,arguments->lock);
    return NULL;
}